import XCTest
@testable import Shifts

class ShiftTest: XCTestCase {
    private let calendar = Calendar(identifier: .gregorian)
    private let formatter = Shift.timeFormatter
    
    func testSameDayShift() {
        let start = calendar.startOfDay(for: Date())
        
        let end = calendar.date(byAdding: DateComponents(hour: 23, minute: 59), to: start)
        let shift = Shift(id: 0, start: start, end: end, imageURL: "/")
        
        let timeRange = shift.formattedTimeRange(ongoing: "Ongoing", day: "day")
        
        XCTAssertEqual(
            timeRange,
            "\(formatter.string(from: start)) - \(formatter.string(from: end!))"
        )
    }
    
    func testPlusOneDayShift() {
        let start = calendar.startOfDay(for: Date())
        let end = calendar.date(byAdding: .day, value: 1, to: start)
        let shift = Shift(id: 0, start: start, end: end, imageURL: "/")

        let timeRange = shift.formattedTimeRange(ongoing: "Ongoing", day: "day")
        
        XCTAssertEqual(
            timeRange,
            "\(formatter.string(from: start)) - \(formatter.string(from: end!)) (+1 day)"
        )
    }
    
    func testPlusTwoDaysShift() {
        let start = calendar.startOfDay(for: Date())
        let end = calendar.date(byAdding: .day, value: 2, to: start)
        let shift = Shift(id: 0, start: start, end: end, imageURL: "/")
        
        let timeRange = shift.formattedTimeRange(ongoing: "Ongoing", day: "d")
        
        XCTAssertEqual(
            timeRange,
            "\(formatter.string(from: start)) - \(formatter.string(from: end!)) (+2 d)"
        )
    }
    
    func testOngoingShift() {
        let start = calendar.startOfDay(for: Date())
        let shift = Shift(id: 0, start: start, end: nil, imageURL: "/")
        
        let timeRange = shift.formattedTimeRange(ongoing: "Ongoing", day: "")
        
        XCTAssertEqual(timeRange, "\(formatter.string(from: start)) - Ongoing")
    }
}
