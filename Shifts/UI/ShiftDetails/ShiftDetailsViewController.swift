import UIKit

public struct ShiftDetailsProperties {
    let shift: String
    let started: String
    let ended: String
    let ongoing: String
}

public final class ShiftDetailsViewController: UIViewController {
    private let shift: Shift
    private let properties: ShiftDetailsProperties
    private lazy var _view = ShiftDetailsView(shift: shift, properties: properties)
    
    public init(shift: Shift, properties: ShiftDetailsProperties) {
        self.shift = shift
        self.properties = properties
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func loadView() {
        view = _view
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        title = "\(properties.shift) \(shift.id)"
    }
}
