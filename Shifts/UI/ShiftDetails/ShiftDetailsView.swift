import UIKit

final class ShiftDetailsView: UIView {
    private let shift: Shift
    private let properties: ShiftDetailsProperties
    private let illustration = UIImageView()
    private let stack = UIStackView()
    
    init(shift: Shift, properties: ShiftDetailsProperties) {
        self.shift = shift
        self.properties = properties
        super.init(frame: .zero)
        
        configure()
        constrain()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        backgroundColor = .white
        
        illustration.backgroundColor = .lightGray
        illustration.contentMode = .scaleAspectFill
        illustration.clipsToBounds = true
        illustration.kf.setImage(with: shift.imageURL)
        
        let started = makeItem(headline: properties.started, body: shift.formattedStartTimestamp)
        let ended = makeItem(
            headline: properties.ended,
            body: shift.formattedEndTimestamp(ongoing: properties.ongoing)
        )
        
        stack.addArrangedSubview(started)
        stack.addArrangedSubview(ended)
        stack.axis = .vertical
        stack.spacing = 16
    }
    
    private func makeItem(headline: String, body: String) -> UIView {
        let headlineLabel = UILabel()
        headlineLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        headlineLabel.text = headline
        
        let bodyLabel = UILabel()
        bodyLabel.font = UIFont.preferredFont(forTextStyle: .body)
        bodyLabel.numberOfLines = 0
        bodyLabel.text = body
        
        let separator = UIView()
        separator.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        separator.constrain(dimension: .height, constant: 1)
        
        let stack = UIStackView(arrangedSubviews: [headlineLabel, bodyLabel, separator])
        stack.axis = .vertical
        stack.spacing = 8
        stack.setCustomSpacing(16, after: bodyLabel)
        
        return stack
    }
    
    private func constrain() {
        addSubview(illustration)
        addSubview(stack)
        
        illustration.constrain(edge: .top, to: safeAreaLayoutGuide, edge: .top)
        illustration.constrain(edges: [.left, .right], to: self)
        illustration.constrain(
            dimension: .height,
            to: safeAreaLayoutGuide,
            dimension: .height,
            relation: .lessThanOrEqual,
            multiplier: 0.38
        )
        
        stack.constrain(edge: .top, to: illustration, edge: .bottom, constant: 16)
        stack.constrain(edges: [.left, .right], to: readableContentGuide)
        stack.constrain(
            edge: .bottom,
            to: readableContentGuide,
            edge: .bottom,
            relation: .lessThanOrEqual
        )
    }
}

extension Shift {
    
    private static let timestampFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .medium
        return formatter
    }()
    
    var formattedStartTimestamp: String {
        return Shift.timestampFormatter.string(from: start)
    }
    
    func formattedEndTimestamp(ongoing: String) -> String {
        return end.map(Shift.timestampFormatter.string) ?? ongoing
    }
}
