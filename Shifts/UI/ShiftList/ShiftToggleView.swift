import UIKit
import RxCocoa

class ShiftToggleView: UIView {
    let toggle: ControlEvent<Void>
    
    var state: Binder<ShiftState> {
        return Binder(self, binding: { $0.update(with: $1) })
    }
    
    private let button = UIButton()
    private let start: String
    private let stop: String
    private let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
    
    init(start: String, stop: String) {
        self.start = start
        self.stop = stop
        self.toggle = button.rx.tap
        super.init(frame: .zero)
        
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .callout)
        button.contentEdgeInsets = .horizontal(8) + .vertical(4)
        button.layer.cornerRadius = 8
        button.layer.masksToBounds = true
        
        addSubview(button)
        button.constrain(edges: .all, to: self)
        
        addSubview(activityIndicator)
        activityIndicator.constrain(centerTo: button)
    }
    
    private func update(with state: ShiftState) {
        let (title, color) = state.currentShift == nil ? (start, #colorLiteral(red: 0.3176470588, green: 0.5921568627, blue: 0.1058823529, alpha: 1)): (stop, #colorLiteral(red: 0.7725490196, green: 0.07450980392, blue: 0.1098039216, alpha: 1))
        updateButton(title: title, color: color, enabled: !state.isLoading)
        button.isHidden = state.error != nil
        activityIndicator.rx.isAnimating.onNext(state.isLoading)
    }
    
    private func updateButton(title: String, color: UIColor, enabled: Bool) {
        button.setTitle(title, for: .normal)
        button.setBackgroundImage(.colorImage(color: color), for: .normal)
        button.setBackgroundImage(.colorImage(color: color.withAlphaComponent(0.5)), for: .disabled)
        button.isEnabled = enabled
        button.titleLabel?.alpha = enabled ? 1 : 0
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
