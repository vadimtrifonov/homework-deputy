import UIKit
import Kingfisher

final class ShiftCell: UITableViewCell {
    private let illustration = UIImageView()
    private let stack = UIStackView()
    private let date = UILabel()
    private let timeRange = UILabel()
    private let height: CGFloat = 62
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configure()
        constrain()
    }
    
    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(with shift: Shift, ongoing: String, day: String) {
        illustration.kf.setImage(with: shift.imageURL)
        date.text = shift.formattedStartDate
        timeRange.text = shift.formattedTimeRange(ongoing: ongoing, day: day)
    }
    
    private func configure() {
        illustration.backgroundColor = .lightGray
        illustration.layer.cornerRadius = height / 2
        illustration.layer.masksToBounds = true
        
        date.font = UIFont.preferredFont(forTextStyle: .headline)
        timeRange.font = UIFont.preferredFont(forTextStyle: .subheadline)
        
        stack.addArrangedSubview(date)
        stack.addArrangedSubview(timeRange)
        stack.axis = .vertical
        stack.distribution = .fillProportionally
    }
    
    private func constrain() {
        contentView.addSubview(illustration)
        contentView.addSubview(stack)
        
        illustration.constrain(edges: .all(except: .right), to: contentView.readableContentGuide)
        illustration.constrain(size: CGSize(width: height, height: height), priority: .maxNonRequired)
        
        stack.constrain(edges: .all(except: .left), to: contentView.readableContentGuide)
        stack.constrain(edge: .left, to: illustration, edge: .right, constant: 16)
    }
}

extension Shift {
    
    private static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .none
        return formatter
    }()
    
    static let timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter
    }()
    
    var formattedStartDate: String {
        return Shift.dateFormatter.string(from: start)
    }
    
    func formattedTimeRange(ongoing: String, day: String) -> String {
        let startTime = Shift.timeFormatter.string(from: start)
        let endTime = end.map(Shift.timeFormatter.string) ?? ongoing
        var range = "\(startTime) - \(endTime)"
        
        if case let plusDays?? = end.map({
            Calendar.current.dateComponents([.day], from: start, to: $0).day
        }), plusDays != 0 {
            range += " (+\(String(plusDays)) \(day))"
        }
        
        return range
    }
}
