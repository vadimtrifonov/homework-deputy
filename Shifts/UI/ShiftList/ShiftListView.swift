import UIKit
import RxSwift
import RxCocoa

final class ShiftListView: UIView {
    let shiftSelected: Driver<Shift?>
    let reload: ControlEvent<Void>
    var state: (_ source: Observable<ShiftState>) -> Disposable {
        return update(with:)
    }
    
    private let properties: ShiftListProperties
    private let tableView = UITableView()
    private let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    private let reloadButton = UIButton()
    private let shiftRelay = BehaviorRelay(value: Optional<Shift>.none)
    
    init(properties: ShiftListProperties) {
        self.properties = properties
        self.shiftSelected = shiftRelay.asDriver()
        self.reload = reloadButton.rx.tap
        super.init(frame: .zero)
        
        configure()
        constrain()
        _ = tableView.rx.modelSelected(Shift.self).takeUntil(rx.deallocated).bind(to: shiftRelay)
    }
    
    private func configure() {
        backgroundColor = .white
        
        reloadButton.setTitle("Reload", for: .normal)
        reloadButton.setTitleColor(.darkText, for: .normal)
        reloadButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: .callout)
        reloadButton.setBackgroundImage(.colorImage(color: #colorLiteral(red: 0.9921568627, green: 0.7450980392, blue: 0.07450980392, alpha: 1)), for: .normal)
        reloadButton.contentEdgeInsets = .horizontal(8) + .vertical(4)
        reloadButton.layer.cornerRadius = 8
        reloadButton.layer.masksToBounds = true
        
        tableView.register(ShiftCell.self, forCellReuseIdentifier: ShiftCell.identifier)
    }
    
    private func constrain() {
        addSubview(activityIndicator)
        addSubview(reloadButton)
        addSubview(tableView)
        
        activityIndicator.constrain(centerTo: self)
        reloadButton.constrain(centerTo: self)
        tableView.constrain(edges: .all, to: self)
    }
    
    private func update(with states: Observable<ShiftState>) -> Disposable {
        return states.do(onNext: { state in
            self.tableView.isHidden = state.shifts.isEmpty || state.error != nil
            self.activityIndicator.rx.isAnimating.onNext(state.isLoading)
            self.reloadButton.isHidden = state.error == nil
            self.selectRow(in: state.shifts)
        })
        .map({ $0.shifts })
        .bind(to: tableView.rx.items(
            cellIdentifier: ShiftCell.identifier,
            cellType: ShiftCell.self
        )) { _, shift, cell in
            cell.update(with: shift, ongoing: self.properties.ongoing, day: self.properties.day)
        }
    }
    
    private func selectRow(in shifts: [Shift]) {
        let indexPath = shiftRelay.value.map(shifts.index)?.map({ IndexPath(row: $0, section: 0) })
        // Select on the next run loop after reload data
        DispatchQueue.main.async {
            self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
