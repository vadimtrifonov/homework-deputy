import UIKit
import RxSwift
import RxCocoa
import CoreLocation

public struct ShiftListProperties {
    let shifts: String
    let ongoing: String
    let day: String
    let error: String
    let ok: String
    let start: String
    let stop: String
}

public final class ShiftListViewController: UIViewController {
    private let interactor: ShiftInteractor
    private let properties: ShiftListProperties
    private let errorFormatter: (Error) -> String
    private let locationProvider: LocationProvider
    private let makeShiftDetails: (Shift) -> UIViewController
    private let bag = DisposeBag()
    
    private lazy var _view = ShiftListView(properties: properties)
    private lazy var shiftToggleView = ShiftToggleView(start: properties.start, stop: properties.stop)
    
    public init(
        interactor: ShiftInteractor,
        properties: ShiftListProperties,
        errorFormatter: @escaping (Error) -> String,
        locationProvider: LocationProvider,
        makeShiftDetails: @escaping (Shift) -> UIViewController
    ) {
        self.interactor = interactor
        self.properties = properties
        self.errorFormatter = errorFormatter
        self.locationProvider = locationProvider
        self.makeShiftDetails = makeShiftDetails
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func loadView() {
        view = _view
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        title = properties.shifts
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: shiftToggleView)
        
        bind()
    }
    
    private func bind() {
        _view.shiftSelected.drive(onNext: { [weak self] shift in
            let shiftDetails = shift.map({ self?.makeShiftDetails($0) })
            shiftDetails?.map({ self?.showDetailViewController($0, sender: self) })
        }).disposed(by: bag)
        
        let reload = _view.reload
            .map({ ShiftAction.reload })
        
        let toggle = shiftToggleView.toggle
            .withLatestFrom(locationProvider.locations)
            .map({ ShiftAction.toggle($0?.geographicCoordinate) })
        
        let actions = Observable.merge(reload, toggle)
            .startWith(.reload)
            .asSignal(onErrorSignalWith: .empty())
        
        let states = interactor.start(actions: actions)
        
        states.drive(_view.state).disposed(by: bag)
        states.drive(shiftToggleView.state).disposed(by: bag)
        
        states.compactMap({ $0.error })
            .drive(onNext: { [weak self] in self?.presentError($0) })
            .disposed(by: bag)
        
        states.compactMap({ $0.message })
            .drive(onNext: { [weak self] in self?.presentMessage($0) })
            .disposed(by: bag)
    }
    
    private func presentError(_ error: Error) {
        let alert = UIAlertController(
            title: properties.error,
            message: errorFormatter(error),
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: properties.ok, style: .cancel, handler: nil))
        presentWithReplace(alert, animated: true)
    }
    
    private func presentMessage(_ message: String) {
        let alert = UIAlertController(
            title: message,
            message: nil,
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: properties.ok, style: .cancel, handler: nil))
        presentWithReplace(alert, animated: true)
    }
}

private extension CLLocation {
    
    var geographicCoordinate: GeographicCoordinate {
        return (coordinate.latitude, coordinate.longitude)
    }
}
