import CoreLocation
import RxSwift
import RxCocoa

public protocol LocationProvider {
    var locations: Driver<CLLocation?> { get }
}

public final class LocationProviderImpl: NSObject, CLLocationManagerDelegate, LocationProvider {
    public let locations: Driver<CLLocation?>
    
    private let locationRelay = BehaviorRelay(value: Optional<CLLocation>.none)
    private let locationManager: CLLocationManager
    
    public init(locationManager: CLLocationManager) {
        self.locationManager = locationManager
        self.locations = locationRelay.asDriver().do(onSubscribe: { [locationManager] in
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        })
        super.init()
        
        locationManager.delegate = self
    }
    
    public func locationManager(
        _ manager: CLLocationManager,
        didUpdateLocations locations: [CLLocation]
    ) {
        locationRelay.accept(locations.last)
    }
}
