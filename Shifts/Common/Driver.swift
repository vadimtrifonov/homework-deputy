import RxCocoa

public extension Driver {
    
    public func compactMap<U>(_ transform: @escaping (E) -> U?) -> Driver<U> {
        return flatMap { element -> Driver<U> in
            transform(element).map(Driver.just) ?? .empty()
        }
    }
}
