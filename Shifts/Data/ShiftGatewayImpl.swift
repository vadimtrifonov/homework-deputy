import Foundation
import RxSwift

public final class ShiftGatewayImpl: ShiftGateway {
    private let apiClient: APIClient
    
    public init(apiClient: APIClient) {
        self.apiClient = apiClient
    }
    
    func fetchBusiness() -> Single<Business> {
        return apiClient.get(path: "/business")
    }
    
    func startShift(coordinate: GeographicCoordinate) -> Single<String> {
        return apiClient.post(path: "/shift/start", parameters: [
            "time": Date().iso8601,
            "latitude": String(coordinate.latitude),
            "longitude": String(coordinate.longitude)
        ])
    }
    
    func stopShift(coordinate: GeographicCoordinate) -> Single<String> {
        return apiClient.post(path: "/shift/end", parameters: [
            "time": Date().iso8601,
            "latitude": String(coordinate.latitude),
            "longitude": String(coordinate.longitude)
        ])
    }
    
    func fetchShifts() -> Single<[Shift]> {
        return apiClient.get(path: "/shifts").catchError({ error in
            if case APIClientError.nullJSON = error {
                return .just([])
            }
            return .error(error)
        })
    }
}

extension Shift {
    
    enum CodingKeys: String, CodingKey {
        case id
        case start
        case end
        case imageURL = "image"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decode(Int.self, forKey: .id)
        start = try values.decode(Date.self, forKey: .start)
        imageURL = try values.decode(URL.self, forKey: .imageURL)
        
        let rawEnd = try values.decode(String.self, forKey: .end)
        end = rawEnd == "" ? nil : try values.decode(Date.self, forKey: .end)
    }
}

private extension Date {
    
    var iso8601: String {
        return ISO8601DateFormatter().string(from: self)
    }
}
