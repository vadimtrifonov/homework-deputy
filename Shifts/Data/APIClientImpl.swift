import Foundation
import RxSwift
import os.log

public enum APIClientError: Error {
    case invalidResponse
    case nullJSON
}

public final class APIClientImpl: APIClient {
    private let baseURL: URL
    private let urlSession: URLSession
    
    public init(baseURL: URL, urlSession: URLSession) {
        self.baseURL = baseURL
        self.urlSession = urlSession
    }
    
    public func get<T : Decodable>(path: String) -> Single<T> {
        let request = URLRequest(url: baseURL.appendingPathComponent(path))
        return dataTask(with: request).map(APIClientImpl.parseResponse)
    }
    
    public func post(path: String, parameters: [String : Any]) -> Single<String> {
        return Single.deferred { [baseURL] in
            try .just(APIClientImpl.makeJSONRequest(baseURL: baseURL, path: path, parameters: parameters))
        }
        .flatMap(dataTask)
        .map(APIClientImpl.parseResponse)
    }
    
    private func dataTask(with request: URLRequest) -> Single<(Data)> {
        return Single.create(
            subscribe: { observer -> Disposable in
                os_log(
                    "%@ REQUEST: %@",
                    request.url?.path ?? "",
                    request.httpBody?.description ?? "EMPTY"
                )
                
                let task = self.urlSession.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        os_log(
                            "%@ ERROR: %@",
                            request.url?.path ?? "",
                            error?.localizedDescription ?? "Unknown"
                        )
                        return observer(.error(error ?? APIClientError.invalidResponse))
                    }
                    
                    os_log(
                        "%@ RESPONSE: %@",
                        request.url?.path ?? "",
                        data.description
                    )
                    observer(.success(data))
                }
                
                task.resume()
                
                return Disposables.create { [weak task] in
                    task?.cancel()
                }
            }
        ).observeOn(MainScheduler.asyncInstance)
    }
    
    private static func makeJSONRequest(
        baseURL: URL,
        path: String,
        parameters: [String: Any]
    ) throws -> URLRequest {
        let url = baseURL.appendingPathComponent(path)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = try JSONSerialization.data(withJSONObject: parameters)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = body
        
        return request
    }
    
    private static func parseResponse<T: Decodable>(data: Data) throws -> T {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        
        do {
            return try decoder.decode(T.self, from: data)
        } catch {
            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            throw json is NSNull ? APIClientError.nullJSON : error
        }
    }
    
    private static func parseResponse(data: Data) throws -> String {
        guard let response = String(data: data, encoding: .utf8) else {
            throw APIClientError.invalidResponse
        }
        return response
    }
}

private extension Data {
    
    var description: String {
        do {
            let json = try JSONSerialization.jsonObject(with: self)
            let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            return String(data: data, encoding: .utf8) ?? ""
        } catch {
            return String(data: self, encoding: .utf8) ?? ""
        }
    }
}

