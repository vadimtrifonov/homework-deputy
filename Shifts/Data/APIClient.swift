import RxSwift

public protocol APIClient {
    func get<T: Decodable>(path: String) -> Single<T>
    func post(path: String, parameters: [String: Any]) -> Single<String>
}
