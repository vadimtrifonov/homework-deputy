import RxSwift
import RxCocoa

public protocol ShiftInteractor {
    func start(actions: Signal<ShiftAction>) -> Driver<ShiftState>
}
