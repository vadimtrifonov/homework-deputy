import Foundation

public struct Business: Decodable {
    public let name: String
    public let logo: URL
}
