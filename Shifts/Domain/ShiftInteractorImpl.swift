import Foundation
import RxSwift
import RxCocoa

public typealias GeographicCoordinate = (latitude: Double, longitude: Double)

public enum ShiftAction {
    case toggle(GeographicCoordinate?)
    case reload
}

public struct ShiftState {
    public var currentShift: Shift?
    public var shifts: [Shift]
    public var error: Error?
    public var message: String?
    public var isLoading: Bool
    
    static var initial = ShiftState(
        currentShift: nil,
        shifts: [],
        error: nil,
        message: nil,
        isLoading: false
    )
}

public final class ShiftInteractorImpl: ShiftInteractor {
    
    enum ShiftResult {
        case shifts([Shift])
        case failure(Error)
        case message(String)
        case loading
    }
    
    private let shiftGateway: ShiftGateway
    
    init(shiftGateway: ShiftGateway) {
        self.shiftGateway = shiftGateway
    }
    
    public func start(actions: Signal<ShiftAction>) -> Driver<ShiftState> {
        let state = BehaviorRelay(value: ShiftState.initial)
        return actions
            .withLatestFrom(state.asSignal(), resultSelector: { ($0, $1) })
            .flatMap(handleAction)
            .scan(state.value, accumulator: ShiftInteractorImpl.reduce)
            .do(onNext: state.accept)
            .asDriver(onErrorDriveWith: .empty())
    }
    
    private func handleAction(_ action: ShiftAction, state: ShiftState) -> Signal<ShiftResult> {
        switch action {
        case let .toggle(coordinate):
            return state.currentShift == nil
                ? startShift(coordinate: coordinate)
                : stopShift(coordinate: coordinate)
        case .reload:
            return fetchShifts()
        }
    }
    
    private func startShift(coordinate: GeographicCoordinate?) -> Signal<ShiftResult> {
        return shiftGateway.startShift(coordinate: coordinate ?? GeographicCoordinate(0, 0))
            .asObservable()
            .map(ShiftResult.message)
            .concat(fetchShifts())
            .startWith(ShiftResult.loading)
            .asSignal(onErrorRecover: { .just(ShiftResult.failure($0)) })
    }
    
    private func stopShift(coordinate: GeographicCoordinate?) -> Signal<ShiftResult> {
        return shiftGateway.stopShift(coordinate: coordinate ?? GeographicCoordinate(0, 0))
            .asObservable()
            .map(ShiftResult.message)
            .concat(fetchShifts())
            .startWith(ShiftResult.loading)
            .asSignal(onErrorRecover: { .just(ShiftResult.failure($0)) })
    }
    
    private func fetchShifts() -> Signal<ShiftResult> {
        return shiftGateway.fetchShifts()
            .asObservable()
            .map({ $0.sorted(by: >) })
            .map(ShiftResult.shifts)
            .startWith(ShiftResult.loading)
            .asSignal(onErrorRecover: { .just(ShiftResult.failure($0)) })
    }
    
    static func reduce(state: ShiftState, result: ShiftResult) -> ShiftState {
        var state = state
        state.error = nil
        state.message = nil
        
        switch result {
        case let .shifts(shifts):
            state.currentShift = shifts.first(where: { $0.end == nil })
            state.shifts = shifts
            state.isLoading = false
            
        case let .failure(error):
            state.isLoading = false
            state.error = error

        case let .message(message):
            state.message = message
            
        case .loading:
            state.isLoading = true
        }
        
        return state
    }
}
