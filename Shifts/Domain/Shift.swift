import Foundation

public struct Shift: Decodable  {
    public let id: Int
    public let start: Date
    public let end: Date?
    public let imageURL: URL
}

extension Shift: Comparable {
    
    public static func == (lhs: Shift, rhs: Shift) -> Bool {
        return lhs.id == rhs.id
    }
    
    public static func < (lhs: Shift, rhs: Shift) -> Bool {
        return lhs.start < rhs.start
    }
}
