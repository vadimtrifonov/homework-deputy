import Foundation
import RxSwift

protocol ShiftGateway {
    func fetchBusiness() -> Single<Business>
    func startShift(coordinate: GeographicCoordinate) -> Single<String>
    func stopShift(coordinate: GeographicCoordinate) -> Single<String>
    func fetchShifts() -> Single<[Shift]>
}
