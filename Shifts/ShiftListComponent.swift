import UIKit
import CoreLocation

struct ShiftListComponent {
    
    func makeShiftList() -> UIViewController {
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.waitsForConnectivity = true
        sessionConfiguration.httpAdditionalHeaders = [
            "Authorization": "Deputy fd4c965c24d66fe735cb85ca5a97b0672dcae74f"
        ]
        
        let session = URLSession(configuration: sessionConfiguration)
        
        let apiClient = APIClientImpl(
            baseURL: "https://apjoqdqpi3.execute-api.us-west-2.amazonaws.com/dmc",
            urlSession: session
        )
        
        let gateway = ShiftGatewayImpl(apiClient: apiClient)
        let interactor = ShiftInteractorImpl(shiftGateway: gateway)
        let locationProvider = LocationProviderImpl(locationManager: CLLocationManager())
        
        let properties = ShiftListProperties(
            shifts: NSLocalizedString("Shifts", comment: "Shift list title"),
            ongoing: NSLocalizedString("Ongoing", comment: "Ongoing shift"),
            day: NSLocalizedString("day", comment: "End date offset in days"),
            error: NSLocalizedString("Error", comment: "Error alert title"),
            ok: NSLocalizedString("OK", comment: "OK button"),
            start: NSLocalizedString("Start", comment: "Start shift button"),
            stop: NSLocalizedString("Stop", comment: "Stop shift button")
        )
        
        let shiftListViewController = ShiftListViewController(
            interactor: interactor,
            properties: properties,
            errorFormatter: { $0.localizedDescription },
            locationProvider: locationProvider,
            makeShiftDetails: { shift in
                let properties = ShiftDetailsProperties(
                    shift: NSLocalizedString("Shift", comment: "Shift details title"),
                    started: NSLocalizedString("Started", comment: "Shift start timestamp label"),
                    ended: NSLocalizedString("Ended", comment: "Shift end timestamp label"),
                    ongoing: NSLocalizedString("Ongoing", comment: "Ongoing shift")
                )
                return ShiftDetailsViewController(shift: shift, properties: properties)
            }
        )
        
        let navigationController = UINavigationController(rootViewController: shiftListViewController)
        let splitViewController = UISplitViewController()
        splitViewController.viewControllers = [navigationController]
        splitViewController.preferredDisplayMode = .allVisible
        
        return splitViewController
    }
}

extension URL: ExpressibleByStringLiteral {
    public typealias StringLiteralType = StaticString
    
    public init(stringLiteral value: StringLiteralType) {
        self.init(string: value.description)!
    }
}
