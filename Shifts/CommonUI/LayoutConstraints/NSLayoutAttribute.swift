import UIKit

public extension NSLayoutAttribute {

	enum Edge {
		case top, left, bottom, right, firstBaseline, lastBaseline
	}

	enum Axis {
		case x, y
	}

	enum Dimension {
		case width, height
	}
}

public extension Array where Element == NSLayoutAttribute.Edge {

	static var all: [NSLayoutAttribute.Edge] {
		return [.top, .left, .bottom, .right]
	}

	static func all(except edge: NSLayoutAttribute.Edge) -> [NSLayoutAttribute.Edge] {
		return all.filter({ $0 != edge })
	}
}

internal protocol LayoutAttributeConvertible {
	var layoutAttribute: NSLayoutAttribute { get }
}

extension NSLayoutAttribute.Edge: LayoutAttributeConvertible {

	var layoutAttribute: NSLayoutAttribute {
		switch self {
		case .top:
			return .top
		case .left:
			return .left
		case .bottom:
			return .bottom
		case .right:
			return .right
		case .firstBaseline:
			return .firstBaseline
		case .lastBaseline:
			return .lastBaseline
		}
	}
}

extension NSLayoutAttribute.Axis: LayoutAttributeConvertible {

	var layoutAttribute: NSLayoutAttribute {
		switch self {
		case .x:
			return .centerX
		case .y:
			return .centerY
		}
	}
}

extension NSLayoutAttribute.Dimension: LayoutAttributeConvertible {

	var layoutAttribute: NSLayoutAttribute {
		switch self {
		case .width:
			return .width
		case .height:
			return .height
		}
	}
}
