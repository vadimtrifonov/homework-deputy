import UIKit

extension UILayoutPriority {
    static let maxNonRequired = UILayoutPriority(UILayoutPriority.required.rawValue - 1)
}
