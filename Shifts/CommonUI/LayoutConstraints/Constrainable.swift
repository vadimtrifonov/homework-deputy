import UIKit

public protocol Constrainable {
}

extension UIView: Constrainable {
}

extension UILayoutGuide: Constrainable {
}

public extension Constrainable {

	// MARK: - Edges

	func constrain(
        edges: [NSLayoutAttribute.Edge],
        to constrainable: Constrainable,
        insets: UIEdgeInsets = .zero
    ) {
		edges.forEach { edge in
			constrain(
				attribute: edge,
				to: constrainable,
				attribute: edge,
				constant: insets.constant(for: edge)
			)
		}
	}

	@discardableResult
	func constrain(
		edge: NSLayoutAttribute.Edge,
		to constrainable: Constrainable,
		edge other: NSLayoutAttribute.Edge,
		constant: CGFloat = 0,
		relation: NSLayoutRelation = .equal,
		priority: UILayoutPriority = .required
	) -> NSLayoutConstraint {
		return constrain(
			attribute: edge,
			to: constrainable,
			attribute: other,
			constant: constant,
			relation: relation,
			priority: priority
		)
	}

	// MARK: - Dimensions

	@discardableResult
	func constrain(
		dimension: NSLayoutAttribute.Dimension,
		constant: CGFloat,
		relation: NSLayoutRelation = .equal,
		priority: UILayoutPriority = .required
	) -> NSLayoutConstraint {
		return constrain(
			attribute: dimension,
			to: nil,
			attribute: nil,
			constant: constant,
			relation: relation,
			priority: priority
		)
	}

	@discardableResult
	func constrain(
		dimension: NSLayoutAttribute.Dimension,
		to constrainable: Constrainable,
		dimension other: NSLayoutAttribute.Dimension? = nil,
		constant: CGFloat = 0,
		relation: NSLayoutRelation = .equal,
        multiplier: CGFloat = 1,
		priority: UILayoutPriority = .required
	) -> NSLayoutConstraint {
		return constrain(
			attribute: dimension,
			to: constrainable,
			attribute: other ?? dimension,
			constant: constant,
			relation: relation,
            multiplier: multiplier,
			priority: priority
		)
	}

	func constrain(
		size: CGSize,
		relation: NSLayoutRelation = .equal,
		priority: UILayoutPriority = .required
	) {
		constrain(dimension: .width, constant: size.width, relation: relation, priority: priority)
		constrain(dimension: .height, constant: size.height, relation: relation, priority: priority)
	}

	// MARK: - Axes

	@discardableResult
	func constrain(
		center axis: NSLayoutAttribute.Axis,
		to constrainable: Constrainable,
		constant: CGFloat = 0,
		priority: UILayoutPriority = .required
	) -> NSLayoutConstraint {
		return constrain(
			attribute: axis,
			to: constrainable,
			attribute: axis,
			constant: constant,
			priority: priority
		)
	}

	func constrain(
		centerTo constrainable: Constrainable,
		constant: CGFloat = 0,
		priority: UILayoutPriority = .required
	) {
		constrain(center: .x, to: constrainable, constant: constant, priority: priority)
		constrain(center: .y, to: constrainable, constant: constant, priority: priority)
	}

	// MARK: - Private

	@discardableResult
	private func constrain<T: LayoutAttributeConvertible>(
		attribute: T,
		to constrainable: Any?,
		attribute other: T?,
		constant: CGFloat,
		relation: NSLayoutRelation = .equal,
        multiplier: CGFloat = 1,
		priority: UILayoutPriority = .required
	) -> NSLayoutConstraint {
        (self as? UIView)?.translatesAutoresizingMaskIntoConstraints = false

		let constraint = NSLayoutConstraint(
			item: self,
			attribute: attribute.layoutAttribute,
			relatedBy: relation,
			toItem: constrainable,
			attribute: other?.layoutAttribute ?? .notAnAttribute,
			multiplier: multiplier,
			constant: constant
		)

		constraint.priority = priority
		constraint.isActive = true
		return constraint
	}
}

private extension UIEdgeInsets {

	func constant(for edge: NSLayoutAttribute.Edge) -> CGFloat {
		switch edge {
		case .top:
			return top
		case .left:
			return left
		case .right:
			return -right
		case .bottom:
			return -bottom
		case .firstBaseline:
			return -bottom
		case .lastBaseline:
			return -bottom
		}
	}
}
