import UIKit

public extension UIViewController {
    
    public func presentWithReplace(_ viewController: UIViewController, animated: Bool) {
        if let presentedViewController = presentedViewController {
            presentedViewController.dismiss(animated: animated) {
                self.present(viewController, animated: animated)
            }
        } else {
            present(viewController, animated: animated)
        }
    }
}
