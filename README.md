# Readme

## Running the project

1) Install [Carthage](https://github.com/Carthage/Carthage)
```
brew install carthage
```

2) Build the dependencies
```
carthage bootstrap --platform iOS
```

## Question answers

### How long did it take you to do? What was your biggest problem?
Around 16 hours. The biggest problem was the lack of UI mockups. The UI had to be figured out on the go with trial and error. I've spent considerable time on UI and UX. The second biggest problem is API.

### What do you think about the API?
It seems to be specifically designed to be hard to work with, especially `/shift/start` and `/shift/end` responses. These responses are in a non-machine readable format, which makes them hard to parse and unreliable when figuring out the state.

### If you had more time, what next feature would you develop?
Aside from map and offline storage I would consider adding reminders about stopping the shift.

## Features implemented

- Shift list
- Shift starting and stopping with geographic coordinate
- Shift details with `UISplitViewController`
- Support of all size classes
- Loading indicators
- Error handling with reload possibility

## Interpretations and assumptions

I've made the following task interpretations and assumptions in my implementation:

- It is acceptable to fetch shifts anew whenever starting or stopping a shift to decrease the ambiguity of state.
- The action to start or stop a shift should be available on the landing screen.
- The start and stop response messages are in a human readable format and can be shown the user.

## Design

### Code structure

The overall code design is loosely based on ideas of Clean Architecture, where view and data are made dependant on the domain (incl. use case) definitions. The project is structured to reflect this idea and make the subsequent split into modules easy.

```
View -> Domain <- Data
```

The groups are:

- Common - foundation layer to be imported by every module
- UI - views and view controllers
- Domain - domain models and use cases
- Data - remote data gateways and stores

**NB!** `LayoutConstraints` is a copy of convenience extensions for Auto Layout from my other projects.

### Event handling

I've decided to implement the task using the idea of unidirectional feedback loops, which works as follows:

1. View receives a user event
2. View propagates the event to the underlying system
3. The system takes the previous state model and transforms it based on the interpretation of the event. *Asynchronous tasks produce multiple state model transformations.*
4. The system propagates the new state model to the view
5. The view updates itself using the new state model. The unidirectional feedback loop is now complete.

```
View(Model(Event))
```
